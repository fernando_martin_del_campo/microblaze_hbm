microBlaze - HBM basic system
======

This small project allows you to create a simple vivado system that then
can be excersized with an SDK project.

The system uses a single microBlaze project connected to a couple channels of the
HBM.

Building
---

 - Create a new project in Vivado

 - In Add Sources, add the block diagram uBlaze_HBM.bd
 
 - In Add constraints, add both xdc files

 - Select the xcvu37p-fsvh2892-2-e-es1 device.

 - Create the HDL wrapper

 - Generate the bitstream

SDK project.
---

 - Generate an SDK new application and use sources/reader.c as the program.

 - Take into account that when you create a new run configuration, you need to change
   the FPGA device from Auto Detect to Digilent JTAG-SMT2NC 210308A65C84/1-xcvu37p
