#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

volatile unsigned int * mem_0  = (unsigned int *)0x80000000;
volatile unsigned int * mem_1  = (unsigned int *)0xA0000000;
volatile unsigned int * mem_2  = (unsigned int *)0xC0000000;
volatile unsigned int * mem_3  = (unsigned int *)0xE0000000;

#define xprint xil_printf

unsigned int index_mem;

int main()
{
    init_platform();


    xprint("Writing to HBM ...\n\r");
    for(index_mem=0; index_mem<10; index_mem++)
    {
        *(mem_0+index_mem) = index_mem;
        *(mem_1+index_mem) = index_mem * 2;
        *(mem_2+index_mem) = index_mem * 3;
        *(mem_3+index_mem) = index_mem * 4;
    }

    xprint("Writing to HBM ...\n\r");
    for(index_mem=0; index_mem<010; index_mem++)
    {
        xprint("Mem_0[%X]: %08x\t Mem_1[%X]: %08x\t Mem_2[%X]: %08x\t Mem_3[%X]: %08x\n\r", 0x80000000 + (index_mem)*4,*(mem_0+index_mem), 0xA0000000 + (index_mem)*4,*(mem_1+index_mem),0xC0000000 + (index_mem)*4,*(mem_2+index_mem), 0xE0000000 + (index_mem)*4,*(mem_3+index_mem));
    }

    xprint("\n\r###########################################################\n\r");


    cleanup_platform();

    return 0;

}
